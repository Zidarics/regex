#!/usr/bin/mawk -f

BEGIN {
    FS=";"
}

{ 
    gsub("/\n/","") 
}

NF==4 && $1 ~ /[a-zA-Z]+/ {
#    st=status($3);
    printf "Name:%s, age:%s, state:%s, died at:%s\n",$1,$2,$3,$4; 
    counts++;
}

END {
    printf "lines:%d, useful:%d\n", NR, counts
}
