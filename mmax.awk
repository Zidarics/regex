#!/usr/bin/awk -f

BEGIN {
    FS=";"; 
    max=0; 
    maxname="?"
}

int($15) > int(max) { 
    max=$15; 
    maxname=$4 
}

#$4 ~/Q.*/ {
#    printf "%s:%d\n",$4,$15;
#}

int($15) > 2000 {
    cnt++;
}

END { 
    printf "most expensive medicine is:%s %s HUF\n", maxname, max;
    printf "there are %d medicines more expensive than 2000 HUF\n", cnt;
}
