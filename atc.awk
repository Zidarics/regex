#!/usr/bin/awk -f
# Find common active ingredient

function usage() {
   print "neither medicine nor atc!\n"
   print "usage: ./atc.awk -v medicine=<find medicine> <database>\n" 
   exit
}

BEGIN { FS=";" 
	IGNORECASE=1
	printf "argc:%d\n", ARGC
	if (ARGC == 1) { 
		usage()
        exit
	}

	printf "echo \"medicine:%s atc:%s\"\n", medicine,atc
	if (atc == "" && medicine== "") {
	   usage()
       exit
	}
}

atc!=""&&$6~atc {
	printf "atc:%s name:%s price:%d\n", $6, $4, $15
}

medicine != "" && $4~medicine {
	printf "%s -> atc:%s, price:%s\n", $4, $6,$15
	ac[$4] = $6
}

END {
    for (i in ac) {
            code = ac[i]
            sub ("[0-9][0-9]$","",code)
            cmd = sprintf("./atc.awk -v atc=%s medicines.csv", code)
            system(cmd)
    }
}











