#!/bin/bash


GREP_FLAGS='-shoP'
SED_FLAGS='-r'

WAIT=1

function wait() {
if [ "$WAIT" = "1" ]; then
   read a
fi
}

function test() {
  local title=$1
  local input=$2
  local pattern=$3
  local last=$4
  [ ! -z "$title" ] && echo "----- $title -----";  echo -e "\u001b[31mecho \"$input\" | egrep $GREP_FLAGS \"$pattern\"\u001b[0m"
  echo -e "input:\t\t$input"
  echo -e "pattern:\t$pattern"
  res=$(echo "$input" | grep $GREP_FLAGS "$pattern")
  echo -e -n "output:\t\t"
  if [ -z "$res" ]; then
     echo -e "\u001b[31mEMPTY\u001b[0m"
  else
     echo -e "\u001b[32m$res\u001b[0m"
  fi
  if [ -z "$last" ]; then
     echo ""
  else
     echo -e "\n"
     wait
  fi
}

function repl() {
  local title=$1
  local input=$2
  local pattern=$3
  local last=$4
  [ ! -z "$title" ] && echo "----- $title -----";  echo -e "\u001b[31mecho \"$input\" | sed $SED_FLAGS \"$pattern\"\u001b[0m"
  echo -e "input:\t\t$input"
  echo -e "pattern:\t$pattern"
  res=$(echo "$input" | sed $SED_FLAGS "$pattern")
  echo -e -n "output:\t\t"
  if [ -z "$res" ]; then
     echo -e "\u001b[31mEMPTY\u001b[0m"
  else
     echo -e "\u001b[32m$res\u001b[0m"
  fi
  if [ -z "$last" ]; then
     echo ""
  else
     echo -e "\n"
     wait
  fi  
}

song="this is the song that never ends
yes, it goes on and on, my friend
some people started singing it
not knowing what it was
and they'll continue singing it forever
just because..."

test "dot(.) metacharacter" "dog" "d.g" 
test "" "dig" "d.g"
test "" "doing" "d.g" "1"
test "Matching specific character" "dig" "d[io]g"
test "" "dog" "d[io]g"
test "" "dag" "d[io]g"
test "" "doing" "d[io]g" "1"
test "Matching number only" "d0g" "d[0-9]g"
test "" "d1g" "d[0-9]g"
test "" "dag" "d[0-9]g"
test "" "doing" "d[0-9]g"
test "" "d1g" "d\dg"
test "" "dag" "d\dg"
test "" "doing" "d\dg" "1"
test "Matching not number only" "d0g" "d[^0-9]g"
test "" "d1g" "d[^0-9]g"
test "" "dag" "d[^0-9]g"
test "" "doing" "d[^0-9]g"
test "" "d1g" "d\Dg"
test "" "dag" "d\Dg"
test "" "doing" "d\Dg" "1"
test "Matching alpha only" "dog" "d[a-zA-Z]g"
test "" "dag" "d[a-zA-Z]g"
test "" "dOg" "d[a-zA-Z]g"
test "" "dAg" "d[a-zA-Z]g"
test "" "d0g" "d[a-zA-Z]g"
test "" "d9g" "d[a-zA-Z]g"
test "" "DAG" "[dD][a-zA-Z][gG]" "1"
test "Matching word only" "dag" "d\wg"
test "" "dOg" "d\wg"
test "" "dAg" "d\wg"
test "" "d0g" "d\wg"
test "" "d9g" "d\wg"
test "" "DAG" "[dD]\w[gG]" "1"
test "Matching not word only" "dag" "d\Wg"
test "" "dOg" "d\Wg"
test "" "dAg" "d\Wg"
test "" "d g" "d\Wg"
test "" "d g" "d\Wg"
test "" "DAG" "[dD]\W[gG]" "1"
test "Matching white spaces" "d g" 'd[ ]g'
test "" "dog" 'd[ ]g'
test "" "doing" 'd[ ]g'
test "" "d g" 'd\sg'
test "Negative look ahead" "apple" "a(?!b)"
test "" "abbey" "a(?!b)" "1"
test "Zero or more repetitions" "dog" "d.*g" 
test "" "dig" "d.*g" 
test "" "doing" "d.*g" 
test "" "dg" "d.*g" "1"
test "One or more repetitions" "dog" "d.+g" 
test "" "dig" "d.+g" 
test "" "doing" "d.+g"
test "" "dg" "d.+g" "1"
test "Zero or more repetitions" "dog" "d.?g" 
test "" "dig" "d.?g" 
test "" "doing" "d.?g"
test "" "dg" "d.?g" "1"
test "n repetitions" "dog" "d.{1}g" 
test "" "dig" "d.{1}g" 
test "" "doing" "d.{1}g"
test "" "dg" "d.{1}g" "1"
test "m to n repetitions" "dog" "d.{1,3}g" 
test "" "dig" "d.{1,3}g" 
test "" "doing" "d.{1,3}g"
test "" "dg" "d.{1,3}g" "1"
test "Greediness" "this is a <b>bold</b>" "<.+>" 
test "" "this is a <b>bold</b>" "<[^>]>"
test "" "this is a <b>bold</b>" "<.+?>"
test "Start of line" "dogs eat chicken" "^d.*g"
test "" "The dogs eat chicken" "^d.*g"
test "End of line" "dogs eat chicken" ".*n$"
test "Start word" "Hey stop the lampstop" "stop\w*"
test "" "Hey stop the lampstop" "\bstop\w*"
test "Beginning a word" "Hey stop the lampstop" "stop\w*"
test "" "Hey stop the lampstop" "\bstop\w*" "1"
test "End of word" "Hey stop the lampstop" "stop\z.*" "1"
test "Word boundary" "This island is beautiful." "\bis\b" "1"
repl "Replace" "President of USA is Donald Trump" "s/Donald Trump/Joe Biden/" "1"
repl "Replace" "Quick dog jumps over the lazy brown fox" "s/^(\w+?\s)(dog)(.*?)(fox)$/\1\4\3\2/" "1"
repl "multiple changes" "$song" "s/on/forward/g" "1"
repl "change only the nth. occurence" "$song"  "s/on/forward/2g" "1"






